// Author: Harry Slaughter <harry@devbee.com>

DESCRIPTION

Simply enforces a minimum size requiremente for user pictures. If a user
submits a picture that is below the minimum requirements, the picture will
be rejected. No auto scaling takes place because the results of scaling images
up are generally not pretty.

I would hope that this parameter will ultimately be included in Drupal core

CONFIGURATION

Configure settings in admin/user/settings
